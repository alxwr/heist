function Write-Message {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        # The message to output
        [String] $Message,

        [Parameter(Mandatory=$false)]
        # The message to output
        [Int] $Length = 73
    )
    $msg = "- " + $Message + ":"
    $msg = $msg + " " * ($Length - $msg.Length)
    Write-Host $msg -NoNewLine
}

Write-Host "********************************************************************************"
Write-Host "Uninstall OpenSSH Script"
Write-Host "********************************************************************************"

$global:ErrorActionPreference = "Stop"
$global:ProgressPreference = "SilentlyContinue"

Write-Message "Ensuring script is run as Administrator"
$Identity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$Principal = New-Object System.Security.Principal.WindowsPrincipal($Identity)
$AdminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator
if (!($Principal.IsInRole($AdminRole))) {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** This script must run as Administrator" -ForegroundColor Red
    exit 1
}
Write-Host "Success" -ForegroundColor Green

$DATA_DIR = "$($env:ProgramData)\ssh"
$INSTALL_DIR = "$($env:ProgramFiles)\OpenSSH"

# Starting with Windows 10 build 1809 and Windows Server 2019 Microsoft included
# 10.0.17763
# OpenSSH as an optional feature
$WIN_VER = (Get-WMIObject Win32_OperatingSystem).version
if ([version]$WIN_VER -ge [version]"10.0.17763") {

    # Uninstall using the Built-in Optional Feature
    Write-Message "Checking OpenSSH feature availability"
    try {
        $server = Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH.Server*'
        if ($server.Count -gt 0) {
            Write-Host "Success" -ForegroundColor Green
        } else {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Package not found" -ForegroundColor Red
            exit 1
        }
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    if ($server.State -ne "NotPresent") {
        Write-Message "Uninstalling OpenSSH feature"
        try {
            $status = Remove-WindowsCapability -Online -Name $server.Name
            Write-Host "Success" -ForegroundColor Green
            if ($status.RestartNeeded) {
                Write-Host "*** Restart needed" -ForegroundColor Yellow
            }
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    } else {
        Write-Message "OpenSSH feature already uninstalled"
        Write-Host "Success" -ForegroundColor Green
        $server
    }
} else {

    # Run the uninstall script
    if (Test-Path "$INSTALL_DIR\uninstall-sshd.ps1") {
        Write-Message "Running OpenSSH Uninstall Script"
        try {
            # I can't suppress output here in Windows 8/2012
            & "$INSTALL_DIR\uninstall-sshd.ps1" *> $null
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }

    # Remove the data dir
    if (Test-Path "$DATA_DIR") {
        Write-Message "Deleting config directory"
        try {
            Remove-Item -Path "$DATA_DIR" -Recurse -Force
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }

    # Remove the instal dir
    if (Test-Path "$INSTALL_DIR") {
        Write-Message "Deleting install directory"
        try {
            Remove-Item -Path "$INSTALL_DIR" -Recurse -Force
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }
}

# Remove firewall rule to allow inbound SSH connections to sshd.exe
if (Get-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -ErrorAction SilentlyContinue) {
    Write-Message "Setting SSH Firewall Rule"
    try {
        Remove-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' | Out-Null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }
}

# Make Powershell the default shell for SSH
if (Test-Path HKLM:\SOFTWARE\OpenSSH) {
    Write-Message "Remove default shell registry entry"
    try {
        Remove-Item -Path HKLM:\SOFTWARE\OpenSSH -Force -Recurse | Out-Null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }
}

Write-Host "********************************************************************************"
Write-Host "OpenSSH Uninstalled Successfully"
Write-Host "********************************************************************************"
