import tempfile
import time

import yaml


def test_default_config(cli_runpy):
    """
    Verify the default config values
    """
    ret = cli_runpy("test", "--config-template")

    assert ret.json.heist.artifact_version == ""
    assert ret.json.heist.artifacts_dir.endswith("artifacts")
    assert ret.json.heist.auto_service is False
    assert ret.json.heist.checkin_time == 60
    assert ret.json.heist.clean is False
    assert ret.json.heist.dynamic_upgrade is False
    assert ret.json.heist.manage_service is None
    assert ret.json.heist.renderer == "yaml"
    assert ret.json.heist.roster is None
    assert ret.json.heist.roster_data is None
    assert ret.json.heist.roster_defaults == {}
    assert ret.json.heist.roster_dir.endswith("rosters")
    assert ret.json.heist.roster_file
    assert ret.json.heist.service_plugin == "raw"
    assert ret.json.heist.target == ""
    # assert ret.json.heist.tunnel_plugin == "asyncssh"


def test_help(cli_runpy):
    """
    Verify that the test heist manager is callable from the cli
    """
    ret = cli_runpy("test", "--help", parse_output=False)
    assert ret.stdout


def test_basic(cli_runpy, roster, openssh_container):
    with tempfile.NamedTemporaryFile(delete=True, mode="w+", suffix=".yml") as fh:
        yaml.safe_dump(roster, fh)
        fh.flush()
        time.sleep(5)
        ret = cli_runpy(
            "test",
            "-R",
            fh.name,
            "--log-level=trace",
            "--output=heist",
            parse_output=False,
        )
    assert "retvalue: 0" in ret.stdout, ret.stderr
    assert "result: Success" in ret.stdout, ret.stderr
