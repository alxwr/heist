import itertools
import os
import tempfile
import uuid
from collections.abc import Generator

import asyncssh
import psutil
import pytest


def used_ports() -> set[int]:
    """
    :return: A set of all the used ports on the local system
    """
    return {x.laddr.port for x in psutil.net_connections()}


def unused_ports() -> Generator[int, None, None]:
    """
    :return: A generator that gets the next unused port from the user range
    """
    for port in itertools.cycle(range(49152, 65535)):
        if port not in used_ports():
            yield port


def random_file(directory: str = None, name: str = None, size: int = 1024) -> str:
    """
    :param name: The text name of the file, defaults to a random string
    :param directory: The directory to create the file in; defaults to /tmp or it's equivalent
    :param size: The amount of random bytes to fill the file with
    :return:
    """
    if not directory:
        directory = tempfile.gettempdir()
    if not name:
        name = str(uuid.uuid4())[:10]
    path = os.path.join(directory, name)
    with open(path, "wb+") as out:
        out.write(os.urandom(size))
    return path


@pytest.mark.asyncio
async def test_create_and_destroy(hub, openssh_container: dict, target_name: str):
    # Setup
    host = openssh_container["host"]
    port = openssh_container["port"]
    username = openssh_container["username"]
    password = openssh_container["password"]

    target = dict(
        host=host,
        port=port,
        username=username,
        password=password,
    )

    # Execute
    assert await hub.tunnel.asyncssh.create(name=target_name, target=target)

    # Verify
    assert hub.tunnel.asyncssh.CONS[target_name].get("con")
    assert hub.tunnel.asyncssh.CONS[target_name].get("sftp")

    # Cleanup, since nothing was accessed the connection will be hanging
    assert await hub.tunnel.asyncssh.destroy(target_name)


async def test_send(hub, openssh_container: dict, temp_dir: str, target_name: str):
    # Setup
    host = openssh_container["host"]
    port = openssh_container["port"]
    username = openssh_container["username"]
    password = openssh_container["password"]
    sftp_root = openssh_container["sftp_root"]
    file_name = "send_example.text"
    f"{sftp_root}/{file_name}"
    source_path = random_file(temp_dir, file_name)
    target = dict(
        host=host,
        port=port,
        username=username,
        password=password,
        known_hosts=None,
    )
    assert await hub.tunnel.asyncssh.create(name=target_name, target=target)
    # Execute

    # Send the temporary file to the root of the sftp server
    assert await hub.tunnel.asyncssh.send(
        name=target_name, source=source_path, dest=file_name
    )

    # Verify
    # THe sftp server is mounted at the temp_dir, verify that the file was put there.
    assert os.path.exists(os.path.join(temp_dir, file_name))


@pytest.mark.asyncio
async def test_get(hub, openssh_container: dict, temp_dir: str, target_name):
    # Setup
    host = openssh_container["host"]
    port = openssh_container["port"]
    username = openssh_container["username"]
    password = openssh_container["password"]
    sftp_root = openssh_container["sftp_root"]
    file_name = "get_example.txt"
    # Create a file in the sftp root
    random_file(directory=temp_dir, name=file_name)
    dest_path = os.path.join(temp_dir, f"{file_name}.copy")

    target = dict(
        host=host,
        port=port,
        username=username,
        password=password,
        known_hosts=None,
    )
    assert await hub.tunnel.asyncssh.create(name=target_name, target=target)

    # Execute
    await hub.tunnel.asyncssh.get(
        name=target_name, source=f"{sftp_root}/{file_name}", dest=dest_path
    )

    # Verify
    assert os.path.exists(dest_path)


@pytest.mark.asyncio
async def test_cmd(hub, openssh_container: dict, target_name: str):
    # Setup
    host = openssh_container["host"]
    port = openssh_container["port"]
    username = openssh_container["username"]
    password = openssh_container["password"]
    command = "echo test"
    target = dict(
        host=host,
        port=port,
        username=username,
        password=password,
        known_hosts=None,
    )
    assert await hub.tunnel.asyncssh.create(name=target_name, target=target)

    # Execute
    ret = await hub.tunnel.asyncssh.cmd(name=target_name, command=command)

    # Verify
    assert ret.stdout.strip() == "test"


# The `@pytest.mark.asyncio` decorator is used in pytest to mark a test function as an asynchronous
# test. This allows the test function to use `await` and `async` keywords to write asynchronous code.
@pytest.mark.asyncio
async def test_tunnel(hub, openssh_container: dict, target_name: str):
    # Setup
    host = openssh_container["host"]
    port = openssh_container["port"]
    remote = port + 1
    username = openssh_container["username"]
    password = openssh_container["password"]

    target = dict(
        host=host,
        port=port,
        username=username,
        password=password,
        known_hosts=None,
    )
    assert await hub.tunnel.asyncssh.create(name=target_name, target=target)

    # Execute
    assert await hub.tunnel.asyncssh.tunnel(name=target_name, local=port, remote=remote)

    # Verify
    async with asyncssh.connect(
        host,
        port=remote,
        username=username,
        password=password,
        known_hosts=None,
    ):
        return
