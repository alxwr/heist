from unittest.mock import call
from unittest.mock import Mock

import pytest


@pytest.mark.asyncio
async def test_start_service(hub, mock_hub):
    """
    Test starting a raw service
    """
    target_name = "1234"
    mock_hub.service.raw.start = hub.service.raw.start
    mock_hub.tool.service = Mock()
    await mock_hub.service.raw.start(
        target_name, "asyncssh", "test-service", "/path/to/run -c /path/to/config"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name,
        "/path/to/run -c /path/to/config",
        background=False,
        target_os="linux",
    )


@pytest.mark.asyncio
async def test_stop_service(hub, mock_hub):
    """
    Test stopping a raw service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.raw.stop = hub.service.raw.stop
    mock_hub.tool.system.os_arch.return_value = ("linux", "amd64")
    await mock_hub.service.raw.stop(target_name, "asyncssh", service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"pkill -f {service_name}", target_os="linux"
    )


@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_status_service(hub, mock_hub, target_os):
    """
    Test getting the status of a raw service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.raw.status = hub.service.raw.status
    mock_hub.tool.system.os_arch.return_value = (target_os, "amd64")
    await mock_hub.service.raw.status(
        target_name=target_name,
        tunnel_plugin="asyncssh",
        service=service_name,
        target_os=target_os,
        sudo=False,
    )
    exp_cmd = f"pgrep -f {service_name}"
    if target_os == "windows":
        exp_cmd = f'powershell -command "get-process {service_name}"'
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, exp_cmd, target_os=target_os, sudo=False
    )


@pytest.mark.asyncio
async def test_restart_service(hub, mock_hub):
    """
    Test restarting a raw service
    """
    target_name = "1234"
    service_name = "test-service"
    run_cmd = "/path/to/binary -c /path/to/config"
    mock_hub.service.raw.restart = hub.service.raw.restart
    mock_hub.tool.system.os_arch.return_value = ("linux", "amd64")
    assert await mock_hub.service.raw.restart(
        target_name, "asyncssh", service_name, run_cmd=run_cmd, target_os="linux"
    )
    mock_hub.service.raw.stop.assert_called()
    mock_hub.service.raw.status.await_count == 2
    mock_hub.service.raw.status.assert_called()
    mock_hub.service.raw.start.assert_called()
