#!/usr/bin/python3
import unittest.mock as mock
from unittest.mock import AsyncMock
from unittest.mock import call

import asyncssh
import pytest
from dict_tools.data import NamespaceDict

import heist.tunnel.asyncssh as asyncssh_tunnel
from tests.helpers import MockAsyncsshCon


def test__get_asyncssh_opt_target(mock_hub):
    """
    Test getting option from the target
    """
    mock_hub.OPT = {"heist": {"username", "opt"}}
    target = {"username": "target"}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "target"


def test__get_asyncssh_opt_config(mock_hub):
    """
    Test getting option from the config if target isn't available
    """
    mock_hub.OPT = NamespaceDict(heist={"username": "opt"})
    target = {}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "opt"


@mock.patch(
    "heist.tunnel.asyncssh._autodetect_asyncssh_opt",
    return_value="autodetect",
)
def test__get_asyncssh_opt_autodetect(mock_hub):
    """
    Test getting option from autodetect of target and config aren't available
    """
    mock_hub.OPT = NamespaceDict(heist={"roster_defaults": {}})
    target = {}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "autodetect"


@pytest.mark.asyncio
async def test_send(hub, mock_hub):
    """
    Test asyncssh.send
    """
    tunnel_name = "12345"
    src = "/tmp/test"
    dest = "/tmp/dest/test"
    mock_hub.tunnel.asyncssh.send = hub.tunnel.asyncssh.send
    mock_hub.tunnel.asyncssh.CONS = {
        tunnel_name: {"sftp": AsyncMock(asyncssh.sftp.SFTPClient), "sudo": False}
    }
    await mock_hub.tunnel.asyncssh.send(tunnel_name, src, dest)
    assert mock_hub.tunnel.asyncssh.CONS[tunnel_name]["sftp"].put.call_args == call(
        src, dest, preserve=False, recurse=False
    )


@pytest.mark.asyncio
async def test_cmd(hub, mock_hub):
    """
    Test asyncssh cmd
    """
    name = "12345"
    mock_hub.tunnel.asyncssh.cmd = hub.tunnel.asyncssh.cmd
    mock_hub.tunnel.asyncssh.CONS = {name: {"sudo": False, "con": AsyncMock()}}
    await mock_hub.tunnel.asyncssh.cmd(name, "echo test")
    assert mock_hub.tunnel.asyncssh.CONS[name]["con"].run.call_args_list == [
        mock.call("echo test")
    ]


@pytest.mark.asyncio
async def test_sudo_cmd(hub, mock_hub):
    """
    Test asyncssh cmd when sudo is True.
    """
    name = "12345"
    mock_hub.tunnel.asyncssh.cmd = hub.tunnel.asyncssh.cmd
    patch_client = mock.patch.object(
        asyncssh.connection, "SSHClientConnection", new_callable=MockAsyncsshCon
    )
    with patch_client as test:
        mock_hub.tunnel.asyncssh.CONS = {
            name: {"sudo": True, "con": test, "password": "securepassword"}
        }
        ret = await mock_hub.tunnel.asyncssh.cmd(name, "echo test")
    assert ret.stdout == "test"
    assert ret.returncode == 1


@pytest.mark.asyncio
async def test_sudo_passwordless_cmd(hub, mock_hub):
    """
    Test asyncssh cmd when sudo is True.
    """
    name = "12345"
    mock_hub.tunnel.asyncssh.cmd = hub.tunnel.asyncssh.cmd
    patch_client = mock.patch.object(
        asyncssh.connection, "SSHClientConnection", new=MockAsyncsshCon(password=False)
    )
    with patch_client as test:
        mock_hub.tunnel.asyncssh.CONS = {
            name: {"sudo": True, "con": test, "password": "securepassword"}
        }
        ret = await mock_hub.tunnel.asyncssh.cmd(name, "echo test")
    assert ret.stdout == "test"
    assert ret.returncode == 1
