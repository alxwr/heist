import pytest


@pytest.mark.parametrize(
    "target_os,exp_ret", [("windows", ".zip"), ("linux", "tar.xz")]
)
def test_get_artifact_suffix(hub, mock_hub, target_os, exp_ret):
    """
    Test function get_artifact_suffix
    for linux and windows
    """
    mock_hub.tool.artifacts.get_artifact_suffix = hub.tool.artifacts.get_artifact_suffix
    ret = mock_hub.tool.artifacts.get_artifact_suffix(target_os=target_os)
    assert ret == exp_ret
