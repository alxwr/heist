import pytest


@pytest.fixture(scope="function", autouse=True)
async def clean_target(hub, target_name: str):
    """
    After each test clean up the connections
    """
    yield
    try:
        await hub.tunnel.asyncssh.destroy(target_name)
    except:
        ...
