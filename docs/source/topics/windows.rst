========================
Heist Support on Windows
========================

Heist uses the SSH protocol to create the communication tunnel to systems it
manages. SSH must be installed and listening on systems to be managed with
Heist.

Starting with Windows 10 build 1809 and Windows Server 2019, OpenSSH is included
as an Optional Feature. Follow the instructions from Microsoft to enable OpenSSH
via `Windows Settings`_ or `Powershell`_.

On older versions of Windows you'll need to install OpenSSH from a third-party
vendor.

If you are provisioning new systems to be managed by Heist, you will need to
create a script that will do the following:

- Install or Enable OpenSSH
- Open Firewall Ports
- Start the OpenSSH Service

This script should be set to run when the machine is provisioned. Most cloud and
vm providers have methods for mechanisms a script on first boot.

Lucky for you, we have created a powershell script for you! Feel free to modify
this script as needed. This script will install OpenSSH using the Optional
Features on newer versions of Windows. On older versions without the Optional
Features it will install OpenSSH from the `Win32-OpenSSH`_ project. The script
will also open the firewall and start the SSH service. The script can be found
`here`_.

.. _`Windows Settings`: https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse#install-openssh-using-windows-settings
.. _`Powershell`: https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse#install-openssh-using-powershell
.. _`Win32-OpenSSH`: https://github.com/PowerShell/Win32-OpenSSH
.. _`here`: https://gitlab.com/saltstack/pop/heist/-/blob/master/heist/scripts/windows/install_openssh.ps1
