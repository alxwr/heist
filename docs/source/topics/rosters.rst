=======
Rosters
=======

Rosters is the system that is used to define the target systems to create
connections to with Heist. The default roster system is called `flat` and
uses the POP ``rend`` system to render the datasets.

.. note::

    By using the ``flat`` roster you can make roster files using yaml, json,
    toml etc. and template the files making it easy to allow for logic to make
    larger lists easier. Don't worry! You don't need to know anything about
    ``rend`` to use rosters. Just know that there is a robust system under the
    hood to make your life easier!

Defining a basic roster is easy:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      password: foobar

In this roster we are using the default yaml ``rend`` system. It is also very
simple because it is just a password login. Heist supports logging into systems
using virtually any login mechanism available to SSH. The options are mapped
directly to asyncssh and can be found here:
https://asyncssh.readthedocs.io/en/latest/api.html#asyncssh.SSHClientConnectionOptions

You can change the ``rend`` system used to render the rosters by setting the
``renderer`` option. By default this is set to yaml.


Common Roster Options
=====================

Use password authentication:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      password: foobar


Using an SSH key for authentication:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      client_keys:
        - /path/to/ssh/key


Using an SSH key with a passphrase for authentication:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      client_keys:
        - /path/to/ssh/key
      passphrase: "password"


Using sudo:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      password: foobar
      sudo: True

Using sudo with a tty. You need to set `tty` to True if you are using sudo and a password.
Heist will interactively input the password defined in the roster when sudo asks for a password.
If you have NOPASSWD set in /etc/sudoers for the defined user you do not need to set `tty` to True
unless requiretty is set on your target. Here is an example:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      password: foobar
      sudo: True
      tty: True


Using sudo with a tty while defining the term_type and term_size. The `term_type` and `term_size` are defining
AsyncSSH's `term_type` and `term_size` options. The `term_type` defines the terminal type to use for the session.
By default this is `xterm-color`. The `term_size` defines the terminal width and height in characters. By default
`term_size` is set to (80,24). Here is an example of setting both `term_type` and `term_size`:

.. code-block:: yaml

    192.168.0.24:
      username: harry
      password: foobar
      sudo: True
      tty: True
      term_type: xterm
      term_size: (80, 24)


Roster Defaults
===============

If you need to set roster options to be used for all hosts you are targeting you can
set `roster_defaults` in the `heist` configuration file. The heist configuration file
by default is at /etc/heist/heist.conf.

.. code-block:: json

    {"roster_defaults":
      {"username": "root"}}


List of Available Rosters
=========================
.. toctree::
  ../ref/rosters/index

Roster Data
===========
Heist allows you to pass in roster data without using a pre-definied roster file. The config
`roster-data` allows a user to pass in json data to be used for the roster data.

.. code-block:: bash

   heist salt.minion --roster-data='{"test1":{"host":"192.168.1.2","username":"root","password":"hostpasswd"}}'

The above command will use the host, username and password defined in the json data passed in
with `--roster-data`. If you also use `roster-file` alongside `--roster-data`, heist will write
the roster data to the specified roster file. For example:

.. code-block:: bash

   heist salt.minion --roster-data='{"test1":{"host":"192.168.1.2","username":"root","password":"hostpasswd"}}' --roster-file=/tmp/heist-roster

This will write the data from `--roster-data` to the file /tmp/heist-roster.
