#!/usr/bin/python3
import json

import pytest
import rend.exc


@pytest.mark.asyncio
async def test_read(mock_hub, hub):
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    roster = "flat"
    ready = {"id_0": {}, "id_1": {"id": "id_1"}}
    expected = {"id_0": {"id": "id_0"}, "id_1": {"id": "id_1"}}
    mock_hub.roster.flat.read.return_value = ready

    # Execute
    result = await mock_hub.roster.init.read(roster)

    # Verify
    mock_hub.roster.flat.read.assert_called_once()
    assert result == expected


@pytest.mark.asyncio
async def test_read_default(mock_hub, hub):
    """
    Test flat is deafult roster when roster
    is not set.
    """
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    ready = {"id_0": {}, "id_1": {"id": "id_1"}}
    expected = {"id_0": {"id": "id_0"}, "id_1": {"id": "id_1"}}
    mock_hub.roster.flat.read.return_value = ready

    # Execute
    result = await mock_hub.roster.init.read()

    # Verify
    mock_hub.roster.flat.read.assert_called_once()
    assert result == expected


@pytest.mark.asyncio
async def test_read_error(mock_hub, hub):
    """
    test heist.roster.init.read error checking
    """
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    roster = "flat"
    ready = {"{% test %}"}
    mock_hub.roster.flat.read.return_value = ready

    # Execute
    try:
        await mock_hub.roster.init.read(roster)
        assert pytest.raises(ValueError, lambda _: _)
    except ValueError:
        ...

    # Verify
    mock_hub.roster.flat.read.assert_called_once()


@pytest.mark.asyncio
async def test_read_id_missing(mock_hub, hub):
    """
    test heist.roster.init.read error checking
    """
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    roster = "flat"
    ready = {"user": "heist", "password": "passwd"}
    mock_hub.roster.flat.read.return_value = ready

    # Execute
    try:
        await mock_hub.roster.init.read(roster)
        assert pytest.raises(ValueError, lambda _: _)
    except ValueError:
        ...

    # Verify
    mock_hub.roster.flat.read.assert_called_once()


@pytest.mark.asyncio
async def test_read_read_empty(mock_hub, hub):
    """
    test heist.roster.init.read when rend returns empty
    """
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    roster = "flat"
    ready = {}
    mock_hub.roster.flat.read.return_value = ready

    # Execute
    try:
        await mock_hub.roster.init.read(roster)
        assert pytest.raises(ValueError, lambda _: _)
    except ValueError:
        ...

    # Verify
    mock_hub.roster.flat.read.assert_called_once()


@pytest.mark.asyncio
async def test_read_rend_exc(mock_hub, hub):
    """
    test heist.roster.init.read when a rend.exc is raised
    """
    mock_hub.roster.init.read = hub.roster.init.read
    # Setup
    roster = "flat"
    mock_hub.roster.flat.read.side_effect = rend.exc.RenderException("Jinja error '}'")

    # Execute
    try:
        await mock_hub.roster.init.read(roster)
        assert pytest.raises(ValueError, lambda _: _)
    except rend.exc.RenderException:
        ...

    # Verify
    mock_hub.roster.flat.read.assert_called_once()


def test_generate_roster_data_no_roster_file(mock_hub, hub):
    """
    test generate_roster_data when not passing
    in roster file as well
    """
    mock_hub.roster.init.generate_roster_data = hub.roster.init.generate_roster_data
    mock_hub.rend.json.render = hub.rend.json.render
    data = {
        "test1": {"host": "192.168.1.59", "username": "root", "password": "password"}
    }
    assert mock_hub.roster.init.generate_roster_data(json.dumps(data)) == data


def test_generate_roster_data_roster_file(mock_hub, hub, tmp_path):
    """
    test generate_roster_data when passing
    in roster file as well
    """
    mock_hub.roster.init.generate_roster_data = hub.roster.init.generate_roster_data
    mock_hub.rend.json.render = hub.rend.json.render
    data = {
        "test1": {"host": "192.168.1.59", "username": "root", "password": "password"}
    }
    roster = tmp_path / "roster"
    mock_hub.roster.init.generate_roster_data(json.dumps(data), roster_file=roster)
    with open(roster) as fp:
        ret = json.load(fp)
    assert ret == data
