#!/usr/bin/python3
"""
    tests.unit.tool.system
    ~~~~~~~~~~~~~~

    tests for tool.system code
"""
import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.asyncio
async def test_os_arch_linux(mock_hub, hub):
    """
    test os_arch when on linux 64 box
    """
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {
            "returncode": 0,
            "stderr": 0,
            "stdout": "linux-gnu|||x86_64-redhat-linux-gnu|||$env:PROCESSOR_ARCHITECTURE\n",
        }
    )
    mock_hub.tool.system.os_arch = hub.tool.system.os_arch
    ret = await mock_hub.tool.system.os_arch("test_name", "asyncssh")
    assert ret == ("linux", "amd64")


@pytest.mark.asyncio
async def test_os_arch_windows(mock_hub, hub):
    """
    test os_arch when on windows 64 box
    """
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {
            "returncode": 0,
            "stderr": 0,
            "stdout": "||||||AMD64",
        }
    )
    mock_hub.tool.system.os_arch = hub.tool.system.os_arch
    ret = await mock_hub.tool.system.os_arch("test_name", "asyncssh")
    assert ret == ("windows", "amd64")
