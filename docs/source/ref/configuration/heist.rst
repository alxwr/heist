.. _configuration-heist:

=================
Configuring Heist
=================

The configuration file for Heist is located at ``/etc/heist/heist.conf`` on
Linux and ``C:\ProgramData\heist\heist.conf`` on Windows by default. The
``C:\ProgramData`` portion of the path might be different in your environment if
your ``ProgramData`` environment variable is set to a different path. If the
``ProgramData`` environment variable is not set on Windows, then Heist will use
``C:\ProgramData`` by default.

The heist.conf file contents need to be under a ``heist`` yaml dictionary. For
example:

.. code-block:: yaml

   heist:
     dynamic_upgrade:True
     checkin_time: 5

You can change the location of the Heist configuration file two ways:

cli opts
========

-c
--

You can pass ``-c /opt/heist.conf`` on the cli when running Heist. This
example would use the file ``/opt/heist.conf`` for the Heist configuration file.


--manage-service
----------------

Heist can manage the service of an artifact that was previously deployed.
The allowed options are start, stop, restart, status, enable and disable.
This argumment will manage the service and then close the Heist connection.

.. code-block:: bash

   heist <manager> -R /etc/heist/roster --manage-service=start

--clean
----------------

If there is a previously deployed artifact on the target, Heist will clean
the artifact before re-deploying again. If there was not a previous artifact
deployed it will log an error but continue deploying a new artifact.

.. code-block:: bash

   heist <manager> -R /etc/heist/roster --clean


environment variable
--------------------

You can set the environment variable ``HEIST_CONFIG`` to the path of the
configuration you want to use for Heist.


.. _primary-heist-configuration:

Primary Heist Configuration
===========================

``acct_profile``
----------------

Default: ``default``

The specified named profile to read from encrypted acct files

.. code-block:: yaml

   heist:
     acct_profile: testprofile


``artifacts_dir``
-----------------

Linux Default: ``/var/tmp/heist/artifacts``
Windows Default: ``C:\\ProgramData\\heist\\artifacts``

The location to look for artifacts that will be sent to target systems

.. code-block:: yaml

   heist:
     artifacts_dir: /etc/artifacts/

``roster``
----------

Default: ``None``

The type of roster to use to load up the remote system to tunnel into.
If the file extension of the roster file is ``.fernet`` the default
roster will be the ``fernet`` roster. Otherwise, the default is the
``flat`` roster.

.. code-block:: yaml

   heist:
     roster: scan

``roster_dir``
--------------

Linux Default: ``/etc/heist/rosters``
Windows Default: ``C:\\ProgramData\\heist\\rosters``

The directory to look for roster files when using the
``flat`` roster.

.. code-block:: yaml

   heist:
     roster_dir: /var/rosters

``roster_file``
---------------

Linux Default: ``/etc/heist/roster``
Windows Default: ``C:\\ProgramData\\heist\\roster``

Use a specific roster file. When using the ``flat`` roster
if this option is not used, then the ``roster_dir`` will be
used to find roster files.

.. code-block:: yaml

   heist:
     roster_file: /var/heist/roster

checkin_time
------------

.. _checkin_time:

Default: 60

The number of seconds between checking to see if the managed systems
need to get an updated binary or agent restart.

.. code-block:: yaml

   heist:
     checkin_time: 100

dynamic_upgrade
---------------

Default: ``False``

Heist will detect when new binaries are available and dynamically upgrade
the target systems.

.. code-block:: yaml

   heist:
     dynamic_upgrade: True

``renderer``
------------

Default: ``yaml``

Specify the renderer to use to render heist roster files.

.. code-block:: yaml

   heist:
     renderer: toml

``target``
----------

Default: ``None``

Target used for multiple rosters. This argument is required for
some rosters such as scan and clustershell.

.. code-block:: yaml

   heist:
     target: 10.0.0.2

``artifact_version``
--------------------

Default: ``None``

Version of the artifact to use for heist

.. code-block:: yaml

   heist:
     artifact_version: 3005

``roster_defaults``
--------------------

Default: ``{}``

Default options to use for all rosters. CLI options will override
these defaults.

.. code-block:: yaml

   heist:
     roster_defaults:
         username: testuser

``service_plugin``
------------------

Default: ``raw``

The type of service to use when managing the artifacts service status.

.. code-block:: yaml

   heist:
     service_plugin: systemd

``auto_service``
----------------

Default: ``False``

Attempt to auto detect the service manager to use on start up of service.

.. code-block:: yaml

   heist:
     auto_service: True

``noclean``
-----------

Default: ``False``

If set to ``True`` do not clean the artifact and configs on the target.
If ``False``, the artifact and configs will be removed from the target.

.. code-block:: yaml

   heist:
     noclean: True

``run_dir_root``
----------------

Default: ``False``

Directory location on remote system for root deployment.

.. code-block:: yaml

   heist:
     run_dir_root: /opt/run/
